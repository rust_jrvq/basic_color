use num::Num;
use rand::Rng;
use std::ops::Mul;

// ------------------- COLOR STRUCT ------------------- //
pub struct Color<T: Num> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

// ------------------- IMPLEMENTATIONS ------------------- //
impl<T: Num> Color<T> {
    // Constructors
    pub fn new_from(r: T, g: T, b: T, a: T) -> Color<T> {
        Color { r, g, b, a }
    }
}

impl Color<f64> {
    // Constructors
    pub fn new() -> Color<f64> {
        Color {
            r: 0.0,
            g: 0.0,
            b: 0.0,
            a: 1.0,
        }
    }
    pub fn new_from_name(name: &str) -> Color<f64> {
        match name {
            "red" => Color {
                r: 1.0,
                g: 0.0,
                b: 0.0,
                a: 1.0,
            },
            "green" => Color {
                r: 0.0,
                g: 1.0,
                b: 0.0,
                a: 1.0,
            },
            "blue" => Color {
                r: 0.0,
                g: 0.0,
                b: 1.0,
                a: 1.0,
            },
            "cyan" => Color {
                r: 0.0,
                g: 1.0,
                b: 1.0,
                a: 1.0,
            },
            "magenta" => Color {
                r: 1.0,
                g: 0.0,
                b: 1.0,
                a: 1.0,
            },
            "yellow" => Color {
                r: 1.0,
                g: 1.0,
                b: 0.0,
                a: 1.0,
            },
            _ => Color {
                r: 0.0,
                g: 0.0,
                b: 0.0,
                a: 1.0,
            },
        }
    }
    pub fn new_rand() -> Color<f64> {
        let mut rng = rand::thread_rng();
        Color {
            r: rng.gen_range(0.0..=1.0),
            g: rng.gen_range(0.0..=1.0),
            b: rng.gen_range(0.0..=1.0),
            a: 1.0,
        }
    }

    // Constructors (pre-defined colors)
    pub fn red() -> Color<f64> {
        Color {
            r: 1.0,
            g: 0.0,
            b: 0.0,
            a: 1.0,
        }
    }
    pub fn green() -> Color<f64> {
        Color {
            r: 0.0,
            g: 1.0,
            b: 0.0,
            a: 1.0,
        }
    }
    pub fn blue() -> Color<f64> {
        Color {
            r: 0.0,
            g: 0.0,
            b: 1.0,
            a: 1.0,
        }
    }
    pub fn cyan() -> Color<f64> {
        Color {
            r: 0.0,
            g: 1.0,
            b: 1.0,
            a: 1.0,
        }
    }
    pub fn magenta() -> Color<f64> {
        Color {
            r: 1.0,
            g: 0.0,
            b: 1.0,
            a: 1.0,
        }
    }
    pub fn yellow() -> Color<f64> {
        Color {
            r: 1.0,
            g: 1.0,
            b: 0.0,
            a: 1.0,
        }
    }

    // Getters
    pub fn coords_rgb_u8(&self) -> [u8; 3] {
        [
            (self.r * 255 as f64) as u8,
            (self.g * 255 as f64) as u8,
            (self.b * 255 as f64) as u8,
        ]
    }
}

// TRAITS
impl<T: Num> Mul<f64> for Color<T>
where
    T: Mul<f64, Output = T>,
{
    type Output = Color<T>;

    fn mul(self, rhs: f64) -> Color<T> {
        Color {
            r: self.r * rhs,
            g: self.g * rhs,
            b: self.b * rhs,
            a: self.a * rhs,
        }
    }
}

impl<T: Num> Mul<Color<T>> for f64
where
    f64: Mul<T, Output = T>,
{
    type Output = Color<T>;

    fn mul(self, rhs: Color<T>) -> Color<T> {
        Color {
            r: self * rhs.r,
            g: self * rhs.g,
            b: self * rhs.b,
            a: self * rhs.a,
        }
    }
}
